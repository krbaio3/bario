const express = require('express');
const { resolve } = require('path');
const { log } = require('console');

const STATIC_FILES_PATH = process.env.STATIC_FILES_PATH || '../dist';

const app = express();
const staticFilesPath = resolve(__dirname, STATIC_FILES_PATH);
app.use('/', express.static(staticFilesPath));

const PORT = process.env.PORT || 8081;
app.listen(PORT, () => {
  log(`App running on http://localhost:${PORT}`);
});
